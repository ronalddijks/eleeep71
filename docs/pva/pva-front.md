# Frontmatter

## Informatie

|                    |                  |
| ------------------ | ---------------- |
| Projectleider      | Stefan Zuithoff  |
| Projectleden       | Nathan Godefroij   |
|                    | Randy Jacobs     |
|                    | Pablo Passchier  |
|                    | Stefan Zuithoff  |
| Begeleider         | J.G. Straver     |
| Datum van uitgifte | 26-9-2019       |

## Versiehistorie

| Versie | Datum      | Wijzigingen              | Auteur       |
| ------ | ---------- | ------------------------ | ------------ |
| 1.0    | 12-09-2019 | Eerste versie opgesteld. | projectgroep |
| 1.1    | 19-09-2019 | Aanpassingen feedback. | Stefan Zuithoff |
| 1.2    | 25-09-2019 | Aanpassingen feedback. | projectgroep |
| 1.3    | 26-09-2019 | Aanpassingen feedback Fogarty. | Stefan Zuithoff |

