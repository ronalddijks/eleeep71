---
title: 'Minor Project: Energieopslag - Plan van aanpak'
---

# 1. Achtergronden

Vanuit de studie Elektrotechniek is er een project opgesteld voor de minor studenten van de Hogeschool Rotterdam. De opdracht wordt gegeven vanuit de Hogeschool Rotterdam zelf en het daadwerkelijke product zal aan de Hogeschool worden opgeleverd. De opdrachtgever vanuit de Hogeschool is dhr. P. Fogarty, docent aan de opleiding elektrotechniek. De projectgroep bevat een hyper multidisciplinaire samenstelling. De projectleden komen uit de volgende opleidingen: Elektrotechniek, Informatica, Civiele Techniek en Chemische Technologie.

Vanuit de Hogeschool Rotterdam is de intentie om een energieneutraal systeem te gebruiken. Dit als een test om duurzame energiebronnen te gebruiken voor practica en projecten van studenten.

Er wordt gewerkt vanuit het resultaat van een vorig project, het bestaande onderzoek wordt meegenomen en op voortgebouwd. Verder wordt er door de opdrachtgever verwacht dat er een nieuw ontwerp gerealiseerd en getest wordt.


# 2. Projectformulatie

## 2.1 Probleemstelling

De Hogeschool Rotterdam heeft op de locatie Academieplein een set aan practicumlokalen, deze heten samen het Technopark. De lokalen worden door verschillende opleidingen gebruikt. De opleiding elektrotechniek maakt gebruik van de lokalen voor het geven van praktijk lessen en het testen en realiseren van projecten. 

De Hogeschool Rotterdam heeft op dezelfde locatie ook beschikking over zonnepanelen, deze staan op het dak geinstalleerd. De zonnepanelen kunnen voor veschillende doeleinden gebruikt worden. In veel situatie's worden zonnepanelen gebruikt om energie terug te leveren aan het net. Een andere mogelijkheid is het intern verbruik van de opgewekte energie, hierbij moet gedacht worden aan een autonoom PV-systeem.

Het idee van de Hogeschool Rotterdam is om een aantal zonnepanelen toe te passen voor intern verbruik. De bovengenoemde practica lokalen van de opleiding elektrotechniek worden voor een deel gevoed door de zonnepanelen. Dit is een eerste stap richting energieneutraliteit. Het gebruik van zonnepanelen vraagt echter om een infrastructuur, welke momenteel ontbreekt. 
 
## 2.2 Doelstelling

Het doel van dit project is om een PV-systeem te ontwerpen en te realiseren dat door de Hogeschool Rotterdam gebruikt kan worden. Het PV-systeem is een vrijwel autonoom systeem dat toegepast wordt in de praktijklokalen van de opleiding elektrotechniek. Omdat het project een test is moet er rekeningen gehouden worden met de schaalbaarheid van het systeem.

# 3. Projectactiviteiten

De projectactiviteiten bestaan uit het onderzoeken, ontwerpen, realiseren en testen van het systeem. Het project wordt uitgevoerd m.b.v. het V-model, dit model heeft invloed op de werkwijze tussen de projectleden en op de projectactiviteiten. Het project is opgedeeld in fases die als resultaat een of meerdere opgeleverde documenten en producten hebben. Iedere week zal er met de projectbegeleider vergaderd worden over de voortgang van het project en de opgeleverde resultaten. Uit de vergaderingen kunnen een paar actiepunten voort komen die verder behandeld worden gedurende de week en in de agenda van de volgende week opnieuw aan tafel komen. Tabel 1 weergeeft de projectactiviteiten voor kwartaal 1.

Tabel 1: projectactiviteiten kwartaal 1 en hun afhankelijkheid.

| code | omschrijving                                      | nodig |
| ---- | ------------------------------------------------- | ----- |
| A    | Overdracht van documentatie vorig project         |       |
| B    | Plan van aanpak                                   |      |
| C    | Plan van eisen                                    |      |
| D    | Acceptatietest                                    | C     |
| E    | Onderzoeksrapport                                 | A     |
| F    | Detail ontwerp deelsystemen                       | E     |
| G    | Projectdocument                                   | C, E & F |

De deelsystemen worden beschreven in het architectuur ontwerp welke beschreven wordt in het projectdocument.

# 4. Projectgrenzen

De visie van een volledig energie infrastructuur dat klimaatneutraal energie levert voor de praktijklokalen in het technopark, wordt niet als realiseerbaar gezien binnen het termijn van dit project. Echter kan een substantieel onderdeel van deze visie wel worden gerealiseerd. Dit onderdeel is een energie buffer die de opgewekte energie opslaat voor later verbruik. Het uiteindelijke doel is om een volledig werkend systeem op te leveren dat in in een later stadium geïntegreerd kan worden in de toekomstige infrastructuur.

Wat wel onder het project valt:

Een belangrijk aspect van dit project is de schaalbaarheid van het systeem. Het te ontwikkelen systeem wordt gerealiseerd op een relatief kleine schaal, echter is het voorzien dat de toekomstige schaal een orde van grootte hoger ligt. Naast het systeem zal er ook een beschrijving en advies over de schaalbaarheid worden geleverd.

Verder valt de compatibiliteit tussen de energie buffer en de zonnepanelen binnen het project. Het fluctuerende gedrag van energieproductie met zonnepanelen kan worden gesimuleerd met behulp van een instelbare voeding. Zo kan de een deel van de integratie en de acceptatie getest worden.

Wat buiten het project valt:

De specificaties van de batterijen en zonnepanelen zijn vooraf bepaald en zijn een belangrijk kader binnen het project. De overige systemen worden deels gebaseerd op deze specificaties echter moet er wel oog zijn voor flexibiliteit vanwege de schaalbaarheid.

Verder valt er buiten het project, de behandeling van energie tijdens overproductie. In het geval dat het opslagsysteem de maximale capaciteit bereikt heeft, zal de energie geproduceerd door de zonnepanelen elders gebruikt moeten worden. Hier zijn meerdere mogelijkheden voor, maar dit valt buiten de scope.

# 5. Tussenresultaten

De tussentijdse resultaten die naar voren komen  in dit project zijn de vereiste documentatie en de gewenste deelsystemen. Hoofdstuk 5.1 en 5.2 beschrijven de verwachten tussenresultaten betreffende documentatie en producten. 

## 5.1 Documenten
- Project planning
- Project eisen
- Architectuur ontwerp
- Onderzoek
- Detail ontwerp
- Unit test
- Integratietest
- Acceptatie test
- Plan voor opschaling (adviesplan)
- Notulen en agenda
- Risico's en planning

## 5.2 Producten
- Batterij management systeem
- DC/DC Converter
- DC-link
- Display/Interface - systeem status

# 6. Kwaliteit

De waarborging van de kwaliteit van de producten die voor dit project geleverd worden is van het belang voor het tijdig voltooien en het voldoen aan de eisen van de opdrachtgever. Hiervoor heeft het projectgroep de volgende stappen uitgezet:

Allereerst is iedereen binnen de groep verplicht gebruik te maken van dezelfde programma’s waarbij de opmaak van de documenten uniform is. Na overleg is gekozen voor Markdown om documenten mee te schrijven. De opmaak is uniform over alle documenten met lettertype Times New Roman en bladzijdenummering.

Om alle digitale producten die gemaakt zijn overzichtelijk te houden en veranderingen per persoon bij te houden wordt er gebruik gemaakt van BitBucket. BitBucket heeft als voordeel dat documenten online staan en dat verschillende versies van hetzelfde document bewaard blijven. Hiermee kunnen projectleden, als er iets fout is gegaan bij het schrijven van een rapport, terugvallen op een oudere versie van hetzelfde document.

Voor het uitvoeren van de testen moet de kwaliteit gewaarborgd worden. Dit wordt binnen het project gedaan door eerst vast te stellen welke handelingen zullen worden uitgevoerd: Welk apparatuur wordt verwacht gebruikt te worden? Welke uitkomsten zijn van te voren berekend? Als dit gedaan is wordt contact opgenomen met één van de docenten om te controleren of alles correct is gepland voordat de test mag beginnen. Als dit niet het geval is worden de fouten verbeterd en wordt de docent nogmaals benaderd om de handelingen door te kijken.

De tests worden uitgevoerd door minimaal 2 projectleden die betrokken zijn geweest bij de voorbereiding en deze hebben doorgelezen. Tijdens de test worden alle handelingen goed bijgehouden, dit kan schriftelijk, met foto’s of zelfs met video. Aan het eind van de test worden alle bevindingen en eventuele problemen tijdens de test gedocumenteerd. De bevindingen zullen worden gecheckt tegenover de berekeningen.

Het belangrijkste onderdeel van het waarborgen van de kwaliteit zal de controle zijn die uitgevoerd zal worden op elke test en eindproduct. Producten dienen minimaal 1 werkdag voor de deadline af te zijn zodat deze gecontroleerd kunnen worden door (minimaal) één ander lid van het projectgroep.

Door deze stappen te ondernemen verwachten wij dat het project tot een geslaagd einde gebracht zal worden.

# 7. Projectorganisatie

De betrokken partijen zijn de projectgroep, de Hogeschool Rotterdam en de projectorganisatie vanuit de opleiding Elektrotechniek. De werklocaties bestaan uit het technopark van locatie academieplein van de Hogeschool Rotterdam en bij projectleden thuis.

## Projectleden

|                |                 |
| -------------- | --------------- |
| Naam           | Stefan Zuithoff (Projectleider)|
| Mobiele nummer | 06 10135869     |
| Mail adres     | 0896209@hr.nl   |

|                |               |
| -------------- | ------------- |
| Naam           | Nathan Godefroij (Notulist) |
| Mobiele nummer | 06 11283009   |
| Mail adres     | 0891198@hr.nl |

|                |               |
| -------------- | ------------- |
| Naam           | Randy Jacobs (Risico analist) |
| Mobiele nummer | 06 37448190   |
| Mail adres     | 0913893@hr.nl |

|                |                 |
| -------------- | --------------- |
| Naam           | Pablo Passchiera (vice Projectleider) |
| Mobiele nummer | 06 53697974     |
| Mail adres     | 0897778@hr.nl   |


## Begeleider

|      |                  |
| ---- | ---------------- |
| Naam | Joris G. Straver |

## Contactpersoon opdrachtgever

|      |              |
| ---- | ------------ |
| Naam | Paul Fogarty |

# 8. Planning

De planning voor het project Energieopslag is gemaakt in het programma Gantt Project. De planning is een strokenplanning die wekelijks besproken wordt. De planning wordt toegevoegd als bijlage.

# 9. Kosten en baten

Het project heeft vanuit de opdrachtgever een EUR 250 budget.

De baten voor de projectleden zitten in de competentieontwikkeling en de voortgang van de studie. De kosten die gemaakt worden tijdens het project worden gedeclareerd bij de Hogeschool Rotterdam.
De Hogeschool Rotterdam komt een stap dichter bij het doel om energieneutraal te zijn door gebruik te maken van duurzame energie. Verder kan dit systeem bijdragen aan het innovatieve imago van de Hogeschool. Ook kan de Hogeschool Rotterdam het ontwerp gebruiken voor toepassingen op andere locaties. Hiervoor tegenover zet de Hogeschool Rotterdam tijd in door een begeleider en contactpersoon beschikbaar te stellen.

#  Bijlagen

- Planning (1_x_Planning_Energieopslag_xx_19.gan)
- Risico's (risicoanalyse.xlsx)