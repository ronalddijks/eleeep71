# Agenda - 2019-09-10

## Aanwezigheid

- Ronald Dijks - 0896559
- Randy Jacobs - 0913893
- Pablo Passchier - 0897778
- Stefan Zuithoff - 0896209
- Khiem Tran - 0890438
  <!-- - Nathan Godefroij - 0891198 -->

# Fogarty

- Wat is het budget?
- Wat is er nog van vorig jaar?
- Wat is het nuttig oppervlakte van het dak van het school.
- Welke bestaande productspecificaties zijn er beschikbaar (denk aan accu's, panelen, etc.).
- Is er al een geplande locatie voor de accu's?
- Kunnen we een aanname doen voor het verbruik van het Technopark, is er al documentatie voor?
