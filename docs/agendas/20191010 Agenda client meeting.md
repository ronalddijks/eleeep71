# Agenda - 2019-10-10

|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 10-10-19 |
| tijd               | N.B.   |
| Aanwezigheid       | Pablo Passchier, Nathan Godefroij, Stefan Zuithoff en Randy Jacobs  |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver  (mogelijk aanwezig)  |
| Klant              | P. Fogarty  |



1. Opening

2. Voortgang
 - Requirements
 - Architectuur

3. Ingekomen stukken
  - Programma van Eisen

4. Rondvraag

5. Afsluiting
