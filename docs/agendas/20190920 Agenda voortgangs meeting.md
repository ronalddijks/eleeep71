|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 20-9-19 |
| tijd               | 12:30   |
| Aanwezigheid       | Randy Jacobs, Pablo Passchier, Nathan Godefroij en Stefan Zuithoff     |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Agenda voor 20 September 2019
1. Opening
2. Voortgang
  - Feedback PvA
  - Projectgroep
  - Verdeling taken
  - Programma van Eisen
  - Acceptatietest
  - Onderzoek


3. Ingekomen stukken
  - Programma van Eisen
  - Planning & Risico
  - PvA


4. Volgende taken
  - Definitief PvE/Acceptatietest
  - Onderzoek
  - Architectuur


5. Rondvraag
6. Afsluiting
