|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 18-10-19 |
| tijd               | 13:30   |
| Aanwezigheid       | Pablo Passchier, Nathan Godefroij, Randy Jacobs en Stefan Zuithoff    |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Agenda voor 18 Oktober 2019
1. Opening

3. Ingekomen stukken
  - Notulen vorige week
  - Architectuur ontwerp
  - Onderzoeksrapport

2. Voortgang
  - Architectuur ontwerp
  - Detail ontwerp
    - BMS systeem
    - Besturingssysteem
    - DC-DC converter

4. Volgende taken
  - Afronden Detail ontwerp (Prio)
  - Afronden Onderzoeksrapport
  - Uitbreiden Acceptatie testen

5. Rondvraag
6. Afsluiting
