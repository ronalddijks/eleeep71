# Agenda - 2019-09-06

## Aanwezigheid

- Ronald Dijks - 0896559
- Randy Jacobs - 0913893
- Pablo Passchier - 0897778
- Stefan Zuithoff - 0896209
- Nathan Godefroij - 0891198
- Khiem Tran - 0890438

# Planning

- Wanneer hebben we een moment met de contactpersoon the praten?
- Welke producten moeten we aanleveren?
- Hoe moeten we de producten aanleveren?
- Hoe zit het met de planning? Er is al een lichaam aan onderzoek, hoe nemen
  we dat mee in deze ronde.
- Kunnen we Projectleider en Notulist per kwartaal kiezen?
- Wanneer moet PVA aangeleverd zijn?
- Wat wordt er verstaan onder persoonlijke competenties, hoe vullen we dat in?

# Fogarthy

- Wat is het budget?
- Wat is er nog van vorig jaar?
- Wat is het nuttig oppervlakte van het dak van het school.
- Welke bestaande productspecificaties zijn er beschikbaar (denk aan accu's,
  panelen, etc.).
- Is er al een geplande locatie voor de accu's?
- Kunnen we een aanname doen voor het verbruik van het Technopark, is er al
  documentatie voor?
