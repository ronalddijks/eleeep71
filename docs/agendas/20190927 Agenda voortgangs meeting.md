|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 27-9-19 |
| tijd               | 13:30   |
| Aanwezigheid       | Randy Jacobs, Pablo Passchier, Nathan Godefroij en Stefan Zuithoff     |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Agenda voor 27 September 2019
1. Opening

3. Ingekomen stukken
  - Plan Van Aanpak

2. Voortgang
  - meeting gehad met Fogarty
  - verandering in scope
  - Programma van Eisen

4. Volgende taken
  - Acceptatietest
  - Onderzoek
  - Architectuur

5. Rondvraag
6. Afsluiting
