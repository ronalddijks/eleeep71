|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 27-9-19 |
| tijd               | 12:15   |
| Aanwezigheid       | Randy Jacobs, Pablo Passchier, Nathan Godefroij en Stefan Zuithoff, Paul Fogharty     |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Agenda voor 26 September 2019
1. Opening

2. Voortgang
  - verandering projectleden
  - architectuur
  - productspecificaties van zonnepannelen
  - accu specificaties
  - display specificaties

3. Rondvraag

4. Afsluiting
