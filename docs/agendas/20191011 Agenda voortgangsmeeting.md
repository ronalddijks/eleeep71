|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 11-10-19 |
| tijd               | 13:30   |
| Aanwezigheid       | Pablo Passchier, Nathan Godefroij en Stefan Zuithoff (Randy Jacobs afwezig)    |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

Randy zal afwezig zijn vanwege een persoonlijke afspraak wat hij aan het begin van het project al medegedeeld had.

#### Agenda voor 11 Oktober 2019
1. Opening

3. Ingekomen stukken
  - Notulen vorige week
  - Programma van Eisen
  - Architectuur ontwerp

2. Voortgang
  - Onderzoeksrapport
  - Acceptatietest
  - BMS systeem (Randy)

4. Volgende taken
  - Afronden Acceptatietest
  - Afronden Onderzoeksrapport
  - Detail ontwerp

5. Rondvraag
6. Afsluiting
