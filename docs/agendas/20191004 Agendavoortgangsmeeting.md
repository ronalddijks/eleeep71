|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 27-9-19 |
| tijd               | 13:30   |
| Aanwezigheid       | Pablo Passchier, Nathan Godefroij en Stefan Zuithoff (Randy Jacobs ziek)    |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Agenda voor 27 September 2019
1. Opening

3. Ingekomen stukken
  - Notulen vorige week
  - Programma van Eisen

2. Voortgang
  - Programma van Eisen
  - Architectuur

4. Volgende taken
  - Acceptatietest
  - Onderzoek
  - Architectuur afronden

5. Rondvraag
6. Afsluiting
