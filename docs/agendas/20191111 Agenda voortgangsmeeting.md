|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 18-10-19 |
| tijd               | 13:30   |
| Aanwezigheid       | Pablo Passchier, Nathan Godefroij, Randy Jacobs en Stefan Zuithoff    |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Agenda voor 18 Oktober 2019
1. Opening

2. GO-noGO

3. Voortgang
- documentatie
  - Onderzoeksrapport
  - Programma van eisen
  - Architectuur

4. Volgende taken
  - acceptatietesten
  - componenten bestellen
  - Meeting met Fogarty -> bestellen Accu's

5. Rondvraag
6. Afsluiting
