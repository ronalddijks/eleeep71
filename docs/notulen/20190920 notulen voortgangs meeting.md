# Notulen vergadering met begeleider 20-09-2019, 12:30

## Aanwezig bij de vergadering
- Mr. Straver
- Randy
- Stefan
- Pablo
- Nathan

## Notulen
1. het PvA
- Datum nog verkeerd
- Fogarty is geen lector
- Hoofdstuknummering
- Probleemstelling is onduidelijk, moet duidelijk genoteerd worden
  - bedoeling was om het lokaal energie neutraal te maken
  - wat is het probleem
  - Hoe gaan we er een oplossing voor maken?
  - Let op wat bij achtergronden horen, en wat bij de probleemstelling hoort.
  - Tekst mag professioneler
- Converter was bij K aangepast, maar bij I nog niet
- Bij Tussenresultaten 3 producten, maar bij programma van eisen veel meer uitgewerkt. Dit meer verwerken in tussenresultaten.
2. Aantal leden verminderd in de projectgroep
- Alles opnieuw verdeeld onder leden
  - voor het electrisch gedeelte
  - 2 voor het programeren gedeelte
- Volgens leraar
  - Alles goed uitplannen en overzicht maken voor welke producten gemaakt moeten worden, en dit onderverdelen
3. Onderzoek
- Bestaande ontwerp onderzoeken
- Simmulatie van de zonnepanelen
- Wegens gebrek van leden minder onderzoek mogelijk
- Kijken of er ook onderzoek gedaan moet worden naar het netspanning.
4.PvE
- Bij PvE
  - Eisnummer onoverzichtelijk
  - Onderverdelen in functionele en niet functionele eisen
  - Template gebruiken
- Systeemconstext moet meer uigebreid worden.
  - Wat zit er wel of niet in?
  - Wat is de context?
  - Goed beeld creeeren van wat er wel of niet in zit
- Algemene omschrijving
  - “Uitgave van stroom”
5. Notulen
- Volgorde aanhouden van de nummering van de agenda
- Aanwezige opschrijven
- Datum en tijd opschrijven
- Aangeven wat het document is (Titel en documentbenaming)
6. Plannen voor volgende week
- Meer duidelijk krijgen van Fogarty over verschillende eisen.
- Duidelijkheid creeeren van het scope van het project.
- Indien tijd over, kijken naar overproductie en hoe dat aan te sluiten op het net.
- PvA en PvE aanpassen
- Onderverdelen Onderzoeken.
- Documenten in de inlevermap stoppen
- Volgend gesprek met Straver 27-09-2019, 13:30

