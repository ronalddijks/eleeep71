|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 27-9-19 |
| tijd               | 13:30   |
| Aanwezigheid       | Randy Jacobs, Pablo Passchier, Nathan Godefroij en Stefan Zuithoff     |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Agenda voor 27 September 2019
1. Opening

2. Ingekomen stukken

  plan van aanpak is duidelijker

3. Voortgang

  bespreken van project changes n.a.v. gesprek fogarthy
  voor ons is zijn de requirements een stuk duidelijker
  scope is veranderd van zonnepaneel tot batterij naar batterij tot grid
  huidig schema is getoond en is goedgekeurd. nadat het volledig uitgewerkt is kan het opgestuurd worden voor Controlle. Evt controle ook bij meneer Forgarty

4. Volgende taken

  waar zijn we mee bezig nu.
  - Acceptatietest
  - Onderzoek
  - Architectuur


5. Rondvraag

  Gevraagd of we de bestandsnamen niet enkel de datum geven maar ook een duidelijke beschrijving.

  vraag: lopen we op schema? nee we lopen achter op schema

  let op competentie ontwikkeling, te vinden in projecthandleiding


Actiepunten:

- aanpassen van bestandsnamen
- Door werken
- zsm PvE afmaken
