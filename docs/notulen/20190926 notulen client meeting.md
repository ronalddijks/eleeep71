|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 26-9-19 |
| tijd               | 12:15   |
| Aanwezigheid       | Randy Jacobs, Pablo Passchier, Nathan Godefroij en Stefan Zuithoff, Paul Fogharty     |
| Notulist           | Randy Jacobs     |

#### Notulen overleg 26-09 met Fogarty
1. Opening

2. Voortgang
 - Verandering projectleden
	Fogarty had nog niet meegekregen dat er 2 leden zijn gestopt met het project
 - Architectuur
	Het is los van het net, er hoeft geen rekening gehouden te worden met een AC/DC converter
	Het gaat om het beheer van de accu's
	Houd rekening met de spanning van het Grid.
	batterij wordt gebruikt om het DC-netwerk te voorzien van een constante spanning.
 - Productspecificaties van Zonnepanelen
	Alleen rekening houden met de maximum productie
	Het gaat om het laden en ontladen
	Ga er van uit dat er een soort omvormer achter de zonnepanelen zit
	De output van de zonnepanelen zullen een soort van stabiele spanning hebben
	Er is geen Solar Charge Controller nodig (MPPT)
 - Accu specificaties
	Batterij worden besteld uitgaande van het onderzoeksrapport van de vorige groep (AGM accu's)
 - Display specificaties
	Historische data is interessant (ook kunnen exporteren)
	Het display is bedoeld dat meerdere mensen het kunnen aflezen
	Grafieken hoeven niet, alleen de data die er in gaat.
	Bedenk een display die eenvoudig is.

	aangeven:
	Wat is de spanning?
	Wat is de state of charge? (mag een increment van 5% zijn)
	aangeven in display: ben je aan het laden of aan het ontladen?
	charge - discharge - idle

	Het display hoeft niet altijd aan te zijn (hoeft niet op netstroom)
	Het kan met LED lampjes gemaakt worden (kleur weergave)
	De display kan op het systeem gemonteerd zijn (op de PCB)

3. Rondvraag
	Het is duidelijk geworden dat de scope verschoven is.
	De scope is nu duidelijker
	veiligheid is alleen voor het systeem zelf bedoeld
