|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 4-10-19 |
| tijd               | 13:30   |
| Aanwezigheid       | Pablo Passchier, Nathan Godefroij en Stefan Zuithoff (Randy Jacobs ziek)    |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Notuelen 4 Oktober 2019
1. Opening
plan van eisen zijn aangepast

3. Ingekomen stukken
  Programma van Eisen:
  requirements zijn duidelijker weergegeven
  haal SMART weg vanwege geen SMART formulering en dat hoeft ook niet
  Enkele spelfouten
  "tussen invoer en uitvoer wordt gemonitord" waar wordt daarmee bedoeld. verbeter de verwoording over DC-Link

  verplaats de systeemfuncties die buiten de scope valt naar de systeem context
  systeemcontext beschrijft het volledige systeem en zegt wat binnen de scope valt
  geef met een plaatje aan wat binnen in de scope valt van het totale systeem

  in systeemfuncties worden alleen de functies besproken waar wij daadwerkelijk aan werken.

  Waar komen deze requirements vandaan? requirements komen van klant en vorige project.
  geef aan waar de specifieke requirements vandaan komen of klant of vorige Onderzoek
  degree sign
  9 volt of 40% over discharge?
  wat is de data die geexporteerd wordt? sample frequentie?
  (0%, 5%, 10%)
  nauwkeurigheid is niet accuratie. geen de accuratie van de data weergave.

  kortsluit beveiliging, rephrase, wat wordt hier concreet aan gedaan.


2. Voortgang
  Stand van het project is nog steeds achter op schema. Waar mogelijk, woensdagen extra werk verrichten


4. Volgende taken
  - Acceptatietest
  - Onderzoek
  - Architectuur
  - Volgende week duidelijkheid hebben over BMS ontwerp van Randy.

5. Rondvraag
  - Vraag hulp wanneer je het nodig hebt.

6. Afsluiting
