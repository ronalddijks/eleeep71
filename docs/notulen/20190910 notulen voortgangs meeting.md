- Fogarthy vertelt wat over de achtergrond.

De droom van de docenten.

- DC grid voor de technopark.
- Zonnepanelen op het dak.
- Energie in een accu stoppen, en tegelijktijdig energie uit de accu kunnen trekken.
- Relatief hoog rendement, rendement is meestal het cijfer waarmee gepronkt wordt.

Verschillende mogelijkheden

- Bidirectionele converters
- Twee converters

Budget

- 250 euro

Simulatie van zonnepanelen

- We gebruiken een variabele voeding om de zonnepanelen te simuleren. Parallel gaat Fogarthy proberen om de panelen op het dak door te trekken naar het technopark.

Documentatie

- We krijgen de documentatie van vorig jaar als boost voor dit project.

Specificaties

- Veiligheid: gebruikelijke normen.
- Accus: zeer voorzichtig mee omgaan, kan je je handen kosten.
- We testen in het practicumlokaal.

Schaalbaarheid

- Het is belangrijk dat het ontwerp wat me maken schaalbaar is, dat we potentieel met factor tien op kunnen schalen.

Overproductie

- We hoeven geen rekening te houden met overproductie van de panelen.
