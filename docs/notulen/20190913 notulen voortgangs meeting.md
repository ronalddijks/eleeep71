# Notulen vergadering met begeleider 13-09-2019

## Aanwezig bij de vergadering
- Ronald Dijks
- Nathan Godefroij
- Randy Jacobs
- Pablo Passchier
- Stefan Zuithoff
- J.G. Straver  

## Comments

- Verkeerde datum op voorblad van PvA.
- Fogarthy is verkeerd gespeld.
- V model toevoegen in PvA.
- Actie punten, vaag.

- Projectactiviteiten zijn niet allemaal activiteiten.
- Onderzoek naar stroom van zonnepanelen, veranderen in onderzoek naar zonnepanelen.
- Stroomconverter van net naar DC niet helemaal duidelijk. Navragen bij fogarthy.
- Bij documenten mist unit tests. Hoort bij detail ontwerp.
- Kwaliteit, er staat een factor voor de boundaries van de testresultaten, die moet eruit.
- Producten die lever je drie dagen voor de deadline in, zou je eerder moeten doen. Zijn het werkdagen of echte dagen?
- Bij organisatie moeten we nog de notulist en projectleider specificeren.
- Herbruikbare energie moet veranderd worden naar duurzame energie.

## Planning voor volgende week

- Bespreken planning voor de volgende week(en).
- Persoonlijk competentie plan hoeft niet meer.
- Agenda moet langer. (bijvoorbeeld voortgang erin zetten.)
- Volgende week vrijdag 15:00 tussengesprek met J.G. Straver.
- Aangepaste PVA 24 uur van te voren.
