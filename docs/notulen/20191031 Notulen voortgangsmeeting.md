|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 4-10-19 |
| tijd               | 13:30   |
| Aanwezigheid       | Pablo Passchier, Nathan Godefroij en Stefan Zuithoff (Randy Jacobs ziek)    |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Notuelen 4 Oktober 2019
1. Opening

2. Ingekomen stukken
  - PvE:
    - Lijkt elke week te verandern, niet specifieke eisen "gekke dingen in PvE".
    - Sommige zinnen kloppen niet.
  - Architectuurontwerp
    - Nieuwe diagram voor processbeschrijving.
    - klopt niet als interconnect diagram, nog erg kaal.
    - context diagram mag wat uitgebreider, uitgave data en aan/uit meer uitwerken.    meer flows gebruiken om het systeem te weergeven.
    - De richting van de Architectuur klopt niet
    - Flowdiagram is karig.
  - Onderzoeksrapport
    - Reeds nog geen tijd gehad om Onderzoeksrapport te lezen. Op eerste gezicht is het onderzoek nog niet echt veel.
    - Maximum charging limit niet in DC DC converter.
    - Maak specifieke onderzoekvragen.
    - Wat is een DC-DC grid.
    - Onderzoek naar de batterijen, wat is de capaciteit, hoever mag het ontladen, hoe werkt de batterij.
    - Microcontrollers, zet de kosten en capabilities van de microcontrollers uiteen. aantal pins bepalen. aantal ADC's.
  - detailontwerp
    - LCD is zo goed als gekozen
    - Type microcontrollers zijn nog onzeker,
    - DC-DC converter
    - BMS er is een keuze gemaakt, voor Randy (beperkt de documentatie van IC keuzes op de 4 a 5 beste opties.)


3. Voortgang

4. Volgende taken
  - PvE duidelijkere eisen opstellen
5. Rondvraag
  - Goed bezig met het ontwerp, documentatie mag beter.
6. Afsluiting
Volgende donderdag 31 om 13:30, Voorkeur Dinsdag middag de documentatie door sturen
