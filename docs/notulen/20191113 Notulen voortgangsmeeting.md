|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 13-11-19 |
| tijd               | 13:30   |
| Aanwezigheid       | Pablo Passchier, Randy Jacobs Nathan Godefroij en Stefan Zuithoff     |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Notulen 13 November 2019

1. Opening

2. GO / NO GO
GO BEHAALD!
Aanmerkingen:
  Te weinig Complexiteit is een zorg
  Zijn de ontwerp keuzes goed onderbouwd?

3. Voortgang
- project document
  - Onderzoeksrapport
  - Programma van eisen
  - Architectuur
  - Detailontwerp

4. Volgende taken
  - acceptatietesten
  - componenten bestellen
  - Meeting met Fogarty -> bestellen Accu's

5. Rondvraag
zorg wat we hebben dat het heelgoed gaat werken vanwege de lage Complexiteit. Goed verantwoorden van keuzes (C vs arduino C)
6. Afsluiting
Volgende vargadering is vrijdag 13:00.
