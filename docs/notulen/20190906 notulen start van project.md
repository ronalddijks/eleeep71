# Notulen gesprek Mr.Staver

- Voorstelrondje
- We krijgen project document, onderzoek van Mr.Staver.
- We gaan een zonnepaneel simuleren met een voeding in het Technopark. Kleine schaal.

# Vast moment in de week voor afspraak?
Vrijdag waarschijnlijk. Mr.Staver stuurt ons een mailtje.

# Hoe moeten we de producten aanleveren?
Via Bitbucket, bijgaand mailtje met wat we willen dat de contactpersoon bekijkt.

# Hoe zit het met de planning? Er is al een lichaam aan onderzoek, hoe nemen we dat mee in deze ronde.
In principe werken we met de normale planning, er is een basis waar we op voort
kunnen borduren. Onderzoeken wat er verbeterd kan worden en hoe we het zelf
aan kunnen pakken.

Fogarthy heeft het idee dat we in week 3 al accus zouden gebruiken. We houden
ons aan de planning van de modulewijzer.

Mogen we SCRUM gebruiken voor de software? Ja, maar we moeten wel onze ontwerpen
verwerken in de SCRUM cyclus. Gehele project in in V-Model, enkel het software 
gedeelte in SCRUM. Zorgen dat het uiteindelijk wel in een document komt voor de 
docent + klant. Verwerken in het PVA.

Wat is de rolverdeling op gebied van verschillende submodules van het project? 
Bespreken met Fogarthy op volgende week dinsdag, Staver kan er bij komen zitten.

# Kunnen we Projectleider en Notulist per kwartaal kiezen?
Nee, liefste een projectleider gedurende het gehele project.
Ook wel handig, bekijk de handleiding voor de punten waarop we beoordeeld worden.

# Wanneer moet PVA aangeleverd zijn?
Volgende week donderdag. Niet vergeten om de risicoanalyse en roadmap bij te voegen.

# Wat wordt er verstaan onder persoonlijke competenties, hoe vullen we dat in?
Plan voor persoonlijke competenties week na de PVA.

# GO/NO GO
Detailontwerp + unittestbeschrijving inleveren.
Het gevolg van een NO-GO is een herkansing.

# Actionable Points
- Uitnodigen voor BitBucket.
- Afspraak maken met Fogarthy Mr.Staver.
- PVA inleveren dinsdag.
- Persoonlijk competentieplan week daarop.