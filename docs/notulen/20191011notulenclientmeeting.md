|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 26-9-19 |
| tijd               | 12:15   |
| Aanwezigheid       | Pablo Passchier, Nathan Godefroij en Stefan Zuithoff, Paul Fogharty (Randy Jacobs afwezig)    |
| Notulist           | Nathan Godefroij     |

#### Notulen overleg 11-10-2019 met Fogarty
1. Opening
Programma van Eisen ontvangen? Weet Fogarty niet meer. Eisen worden besproken.



2. Opmerkingen tijdens bespreken van PvE.
Laad en ontlaad tijd. Zoek balans tussen levensduur en laadtijd
MAX current draw
Voltage leveling als eis toevoegen

beschrijving eis BES003 klopt niet.

architectuurontwerp besproken en bespreking gehad over

requirements over hoe is niet intressant wel de eisen wat het systeem kan en zal doen.

data exporteren met Micro USB heeft voorkeur boven SD-kaart.

Overige eisen voor DC-DC link zijn standaard eisen (Stefan weet welke dat zijn)

Wellicht Wordt het Technopark 24 V, Zonnepanelen leveren 48 Volt. Kijken wat de gevolgen hiervan zijn. mocht dit een probleem zijn dan mogen wij kiezen voor de 48 volt.

2. Voortgang

wat vind u van de voortgang? redelijk, verwacht dat we in staat zijn van detailontwerp.


3. Rondvraag

niet toe te voegen of aan te merken.
