|                    |                  |
| ------------------ | ---------------- |
| Onderwerp          | Vergadering EEP71  |
| Datum              | 4-10-19 |
| tijd               | 13:30   |
| Aanwezigheid       | Pablo Passchier, Nathan Godefroij en Stefan Zuithoff (Randy Jacobs ziek)    |
| Notulist           | Nathan Godefroij     |
| Begeleider         | J.G. Straver     |

#### Notuelen 4 Oktober 2019
1. Opening
- Gesprek gehad met Fogarty
- De programma van eisen is besproken en er zijn extra eisen nodig.

- focus op de detail ontwerp en de Acceptatietest op een later stadium
maak wel een opzet van Acceptatietest.

2. Ingekomen stukken
- PvE:
    - Contextdiagram, omvormer weg, voltage weg uit de voeding, technopark 24 V.

    - BES001 is geen eis, wat wilt de klant hier en definieerd dit beter.

    - BES002 "het vermogen <-> **de wattage**"

    - BES003 beschrijving klopt taalkundig niet,

    - BES005 zin aanpassen niet een volledige zin.   

- architectuurontwerp:
    - Niet ontvangen/ gezien door dhr. Straver.

    - flow van data tussen besturingsysteem en display

    - voltage tussen de bms en batterijen meer  gedetaileerd

    - voeg dataopslag in architectuurontwerp.

    - hou "Jordan" aan in architectuurontwerp en Interconnect Figuur


3. Voortgang

  - Hard doorwerken. meer uren maken. Er moet nog veel worden gedaan.


4. Volgende taken

  - Onderzoek
  - Architectuur
  - Volgende week duidelijkheid hebben over BMS ontwerp van Randy.

5. Rondvraag
  - Vraag hulp wanneer je het nodig hebt.
  - afspraak volgende week dezelfde tijd.

6. Afsluiting
