# Intro
Vanuit de Hogeschool Rotterdam is de intentie om een energieneutraal systeem te gebruiken. Het project dient als een test om duurzame energiebronnen te gebruiken. De opdracht is het ontwerpen van een elektrisch energie opslag systeem voor het technopark, de invoer van energie komt uit zonnepanelen.
\par
Er wordt gewerkt vanuit het resultaat van een vorig project, de methodieken uit het bestaande onderzoek worden geanalyseerd en meegenomen in het huidige onderzoek. De Hogeschool Rotterdam beschikt al over een aantal producten die gebruikt kunnen worden voor het huidige project, dit betreft: accu's, zonnepanelen en meetinstrumenten om het systeem te simuleren en te testen.

## Vooronderzoek
Het PV-systeem dat wordt ontworpen voor de Hogeschool Rotterdam bestaat uit verschillende deelsystemen. In het voorgaande project is het proces uitgewerkt in een architectuurontwerp, zie figuur.


De deelsystemen zichtbaar in figuur ? worden beschreven in het project document. Het proces "Solar" meet de spanning en stroom van het zonnepaneel en stuurt de gemeten waardes door naar de DC-link, de opgewekte energie wordt getransporteerd naar het oplaadcircuit. Het proces "BMS" betreft het op- en ontladen van de accu's. Het oplaadcircuit maakt gebruik van verschillende methodieken om een langere levensduur van de accu's te garanderen, de methodieken zijn: Constant Current - Constant Voltage charging (CC-CV) en trickle charging. Het ontlaadcircuit verzorgt de gewenste spanning aan het grid en geeft de capaciteit van de accu's door aan de DC-link, om diep ontlading te voorkomen. Het proces "DC-link" wisselt tussen verschillende DC-bronnen, communiceert met verschillende onderdelen en bepaald welke bron het grid voedt. Verder is de DC-link verantwoordelijk voor het opladen en de beveiliging van de accu's. Het proces "Display" weergeeft data over het PV-systeem.

Voor elk deelsysteem zijn verschillende ontwerpkeuze's gemaakt


Systeem     Ontwerpkeuze \
Solar                	LTC4151   						
BMS: Ontlaadcircuit 	Boostconverter met LTC3862                        			
BMS: Oplaadcircuit        	Two-step charging, CC-CV charging met LT8490		
Kortsluitbeveiliging          	LM5066I    	                       					
DC-Link: uC  		MSP430G2x53                          				
DC-Link: FET driver}          FET driver geschakeld d.m.v. transistor		
Display}       			I2C									

## Solar
Het proces "solar" houdt zich bezig met het meten van de spanning en stroom van het zonnepaneel en communiceert de gemeten waardes met de DC-link . De LTC4151  is een IC dat zowel de spanning als de stroom kan monitoren. De gemeten waarden worden d.m.v. I2C vertuurd naar de microcontroller . De weerstand (R) tussen SENSE+ en SENSE- is afhankelijk van de maximale stroom die gemeten wordt, deze wordt bepaald door de karakteristieken van de zonnepanelen. De overige weerstanden zijn pull-up weerstanden die de default state van de I2C configureren. De operating range van de LTC4151 is tussen de 7 en 80V

## BMS
LT8490
LTC3862
## Kortsluitbeveiliging
## DC-Link
## Display
