---
title: 'Minor Project: Energieopslag - Onderzoeksrapport'
---

# Voorwoord
In opdracht van de Hogeschool Rotterdam wordt er een off-grid DC-netwerk ontwikkeld. Deze opdracht zal worden uitgevoerd door studenten van de Hogeschool Rotterdam die een minor volgen bij de opleiding Elektrotechniek. De opdrachtgever binnen de Hogeschool is dhr. P. Fogarty en de begeleiding wordt eveneens vanuit de Hogeschool gegeven door dhr. J.G. Straver.

De studenten die dit project uitvoeren en de achtergronden zijn als volgt:

Naam | Functie | Opleiding
---|---|------|
Stefan Zuithoff |  (Projectleider)       |  Elektrotechniek
Randy Jacobs    |  (Risico analist)      |  Civiele Techniek
Pablo Passchiera|  (vice Projectleider)  |  Technische Informatica  
Nathan Godefroij|  (Notulist)            |  Chemische Technologie

# 1. Inleiding
Dit rapport vat de onderzoeksfase samen en vormt de basis van dit project. Een belangrijk aspect van dit rapport is de analyse van het werk dat in een eerdere fase uitgevoerd is. Dit project is in het jaar 2018 begonnen met een ander project team, en wordt in het huidig jaar (2019) voortgezet. De voorgaande fase heeft een onderzoeksrapport en detailontwerp geleverd echter is de realisatie niet geslaagd. Het onderzoeksrapport en detailontwerp van de vorige fase dient als een opstap voor dit project zodat de realisatie eenvoudiger uitgevoerd kan worden.

De wens van de Hogeschool Rotterdam is een off-grid Direct Current (DC) netwerk wat voorzien wordt van duurzaam opgewekte stroom en dit kan leveren aan het Technopark. Als bron van duurzame energie zal zonne energie gebruikt worden. Doordat zonne energie een variabele productie van elektriciteit levert, zal er een buffer-systeem nodig zijn om de spanning op het DC-netwerk constant te houden. Dit buffer-systeem zal de focus zijn van het huidig project.

Met de realisatie van dit project komt Hogeschool Rotterdam een stap dichter bij het doel om energieneutraal te zijn door gebruik te maken van duurzame energie. Verder kan dit systeem bijdragen aan het innovatieve imago van de Hogeschool. Ook kan de Hogeschool Rotterdam het ontwerp gebruiken voor toepassingen op andere locaties.

# 2. Smart-grids
Het te ontwikkelen netwerk voor de Hogeschool Rotterdam zou onder de noemer van "Smart-grid" kunnen vallen. Dit is als begin van het onderzoek gekozen zodat de benodigde componenten gespecificeerd kunnen worden. De volgende karakterestieken scheiden een Smart-Grid van traditionele elektriciteitsnetwerken [1]:

- Een verbeterde omgang met sterk variabele hernieuwbare energy bronnen. De power grid kan verschillende krachtbronstructuren aanpassen.

- Drastische verlaging van het risico op grootschalige stroomstoringen.

- Zou kosten moeten drukken evenals de energie consumptie en emmissies.

Een toevoeging aan deze verschillen is het verschil dat traditionele elektriciteitsnetwerken de productie aanpassen op de momentele vraag. Bij een smart-grid kan de productie van elektriciteit los staan van de vraag door gebruik te maken van opslag technieken.

Dit hoofdstuk bespreekt het type elektriciteitsnetwerk dat exclusief DC stroom gebruikt. Dit type netwerk is hetgeen dat in dit project ontworpen zal worden en vormt daarom een belangrijke achtergrond voor het verdere onderzoek dat meer in detail zal treden.

## 2.1. DC-grids
De grote natinale en internationale elektriciteitsnetwerken maken gebruik van AC vanwege de grote transportafstand. Dit soort netwerken werken zijn nodig als de energieopwekking voornamelijk centraal geregeld is. Echter door de ontwikkeling van hernieuwbare bronnen is de energie productie steeds decentraler geworden. Wanneer de energieproductie decentraal geregeld is, dan bied een DC-grid verschillende voordelen.

Tegenwoordig gebruiken het meerendeel van de apparaten intern een DC-stroom. De stroom uit het netwerk moet eerst door een omvormer van AC naar DC omgezet. Echter is deze omzetting minder efficiënt dan een DC naar DC omzetting. Bijkomend is dat de productie van energie uit hernieuwbare bronnen vaak een DC stroom leveren. Verder hebben Accu's ook een DC werking waardoor een DC-grid steeds meer voordelen bied. De aantal conversie stappen zijn hierdoor drastisch verlaagd, geen DC/AC en AC/DC conversies en frequentie synchronisatie is niet meer nodig [2].

## 2.2 Componenten in een DC-grid
De hoofd componenten in een off-grid DC-grid zijn een en


# 3. Functionele Componenten
Het volgende beschrijft de functionele componenten van een DC-grid in meer detail.

## 3.1. Zonnepanelen
De stroom toevoer voor het DC-grid wordt geproduceerd door zonnepanelen. Zonnepanelen maken gebruikt van een serie van zonnecellen die zonlicht omzetten in elektriciteit. De energie productie van een zonnecel is voornamelijk afhankelijk van de lichtintensiteit, richting van inval, temperatuur [X].

## 3.2. Accu's
De Accu is een passief systeem welk wordt aangesloten op de Battery Management System. Het wordt gevoed vanuit het BMS en kan ontladen worden naar het technopark. De eisen en specificaties zijn voorafgaand aan het project gespecificeerd en valt daardoor buiten de scope van het PvE.

## 3.3. Solar Charge Controller
Een Solar Charge Controller (SCC) is een circuit dat de stroom toevoer vanaf de zonnepanelen moet regelen. De energie productie in een zonnepaneel is sterk variabel door verschillende condities, waaronder temperatuur, hoeveelheid en richting van licht inval. De SSC kan deze stroom toevoer regelen zodat de zonnepaneel meest efficiënt belast wordt. Twee verschillende toepassingen kunnen hiervoor gebruikt worden, dat zijn Pulse Width Modulation (PWM) en Maxiumun Power Point Tracking (MPPT). In meeste gevallen behaald MPPT de hoogste efficiëntie in vergelijking met PWM.

## 3.4. Battery Management System
Aangesloten aan het SCC is het Battery Management System (BMS). De BMS gebruikt de power van de SCC om de Accu's te kunnen opladen. De BMS regelt de afzonderlijke Accu's op verschillende condities waaronder temperatuur, state of charge, state of discharge. De status informatie van de Accu's worden gecommuniceerd aan het besturingsysteem.

## 3.5. DC-DC Omvormer
Om een gelijkspanning te stabiliseren wordt er vaak gebruik gemaakt van lineaire serieregelaars [S1]. De operatie hiervan is gebaseerd op een spanning- of stroomdeler [S2]. De lineaire regelaars worden voornamelijk toegepast in low power applicaties, de volgende kenmerken zijn typerend voor lineaire regelaars [S1][S2]:

- Inefficiënt, rendement tussen 30 en 50%,
- Vout moet lager zijn dan Vin,
- High quality output.

De lineaire serieregelaars zijn dus niet geschikt voor elke applicatie. Een alternatief voor de lineaire serieregelaar is de geschakelde voeding. Geschakelde voedingen worden toegepast bij: motorcontrole, batterijladers, computervoeding en spanning- en stroomstabilisatie voor industrie en labo [S1]. Enkele kenmerken van de geschakelde voeding zijn [S2]:

- Efficiënt, rendement van 70 tot 90%,
- Vout kan hoger en lager zijn dan Vin,
- Het biedt isolatie tussen de source en load,
- Beschermt het systeem tegen EMI (Electromagnetic Interference).

De geschakelde voeding is in verschillende opzichten een betere keuze voor het project dan de lineaire regelaar. De eisen omtrent efficiëntie kunnen alleen behaald worden met de geschakelde voeding. Andere aspecten betreft veiligheid kunnen ook gerealiseerd worden met de geschakelde voeding.

Het basisprincipe van de geschakelde voeding is gebaseerd op een chopper (hakker) en een filter [S1]. Om een regelbare uitgang te verkrijgen wordt PWM toegepast. Het filteren van de chopper uitgang geeft een gestabiliseerde gelijkspanningsvoeding [S1]. Een andere methode van schakelen is soft-switching [S2] echter wordt deze minder vaak toegepast. PWM switching heeft een aantal voordelen [S2]:

- Weining componenten,
- Hoog rendement,
- Constante frequency operation,
- Simpele controle,
- IC controllers,
- Hoge conversion rate.

De nadelen van PWM switching zijn echter [S2]:

- Turn on en off verliezen,
- genereren van EMI (Electromagnetic Interference),
- Rectangular spanning en stroom waveforms.

Voor geschakelde voedingen zijn er drie werkingsprincipes: buck, boost en buck-boost [S1]. De buck converter is een step down converter, de boost converter is een step up converter en de buck-boost converter werkt in beide richtingen [S2].

### 3.5.1. Buck Converter  
In figuur S1 is het principeschema van een buck converter weergegeven [S2]. Hierin is: Vs de ingangsspanning, S de schakelaar, D de diode, L de spoel, C de condensator, R de load weerstand en Vo de uitgangsspanning. De schakelaar is in praktijk vaak een IGBT, MOSFET, Power BJT, GTO of transistor [S2]. De schakelaar wordt aangestuurd door een PWM signaal met een duty cycle $\delta = t_{ON}/T$ [S1]. De spanning na de schakelaar kan beschreven worden als de ingangsspanning vermenigvuldigd met de duty cycle; $U_{ns}=U_s\times\frac{t_{ON}}{T}$ [S1]. De uitgangsspanning van de buck coverter komt ongeveer overeen met $U_o\approx U_{ns}\times \delta$ [S1]. Het principe van een chopper komt hierbij naar voren. Zoals eerder beschreven moet er voor een correcte uitgangsspanning nog een filter geplaatst worden.     

![Image](figuren/buckconverter.PNG "Buck Converter schematic")
**Figure S1 - Principeschema buck converter [S2]**

Het meest eenvoudige filter is een LC-opstelling welke te zien is in figuur S1. Door de hoge schakelfrequentie (kHz) zullen de filter elementen geringe waarden en afmetingen hebben [S1]. De geschakelde voedingen hebben twee standen, de ON en OFF stand. Bij de ON stand is de schakelaar gesloten en bij de OFF stand is de schakelaar open. De buck converter in de ON stand wordt weergegeven in figuur S2 [S3].

![Image](figuren/buckstanden.PNG)
**Figuur S2 - Buck converter in ON en OFF stand [S3]**

Bij het sluiten van de schakelaar (ON) vloeit de stroom IL  en verkrijgen we een Uu met de aangeduide polariteit, de diode staat gesperd [S1]. Door het openen van de schakelaar (OFF) zal de magnetische energie, opgeslage in de kern van de spoel ontladen via C//Rb en D [S1]. De diode is dus een vrijloop diode [S1]. De stroom door de spool kan continu (CCM) of discontinu (DCM) zijn [S1][S2]. Bij een continu stroomverloop (CCM) is de stroom door de spoel (IL) nooit gelijk aan nul, bij een discontinu stroomverloop (DCM) is de stroom door de spoel (IL) gelijk aan nul voordat de schakelaar weer sluit [S1][S2]. CCM heeft een hoger rendement dan DCM en wordt vaker toegepast, DCM is nuttig voor specifieke toepassingen [S2].

### 3.5.1. Simulatie buck converter
Voor het ontwerp van de DC/DC converter zijn de volgende eisen vastgesteld.

- Vin = 48V
- Vout = 24V
- Het rendement = 90%
- Vout ripple is < 1%
- $\Delta$Iout < 20%

De simulatie wordt uitgevoerd in LTSpice. Lineair Technology produceerd IC's die toegepast kunnen worden bij geschakelde voedingen. Het ontwerp van een buck converter met IC zal complexer zijn in productie (PCB design) dan een ontwerp bestaande uit analoge componenten (figuur 1 en 2), maar ook betere eigenschappen leveren (hoger rendement). Voor de simulatie is gekozen voor een LT3840. De LT3840 is een wide input range synchronous regulator controller with acurate current limit [S4]. De input range is van 2.5V tot 60V en de output range is tot 60V. De LT3840 is efficiënt, beschikt over extra beveiligingen, beschikt over een uitgebreide datasheet en er is mogelijkheid tot simulaitie in LTSpice. Voor de juiste toepassing van de LT3840 moeten een aantal component keuze's gemaakt worden.

De eerste keuze betreft de schakel frequentie. Het kiezen van een hogere frequentie resulteert in lagere capacitieve en inductieve waardes. Het kiezen van een lagere frequentie resulteert in minder schakelverliezen en verbeterd de efficiëntie van het ontwerp. Er moet rekening gehouden worden met de minimale on en off tijd bij de gekozen frequentie. De gekozen frequentie is 200kHz, de frequentie wordt ingesteld door het kiezen van een weerstand Rt van 76.8 kOhm [S4]. Een frequentie van 200kHz staat gelijk aan 5us. De dutycycle is een 0.5 volgens de beschreven requirements. De minimale on en off tijd van 150ns en 240ns worden behaald [S4].

De minimale inductie waarde van de spoel kan berekend worden met de volgende formule:
$L\geq Vout\times\frac{Vin(max)-Vout}{fsw\times Vin(max)\times \Delta IL}$

toevoegen:
Cin keuze
Cout keuze
Output Voltage
Output current

Simulatie afbeelding
Simulatie / resultaat.

## 3.6. DC-Link


## 3.7 Besturingsysteem

### 3.7.1 Inleiding
Uit het programma van eisen zijn bepaalde functies bepaald. Deze worden
onderzocht in de context van het architectuurontwerp. De functionele eisen worden samengebracht in een onderzoeksvraag.

Dit onderzoek is onderdeel van de minor Embedded Systems. Hier draait
het om het gebruik van computersystemen als onderdeel van een groter systeem.

### 3.7.2 Analyse
Analyse van de problemen zoals die worden beschreven in het PVE en voortkomen
uit het onderzoek van de vorige porjectgroep.

Voor het aansturen van het BMS moet er aan schaalbaarheid worden gedacht.
Er is een strategie nodig om meerdere BMS te besturen. De communicatie
methode moet hiervoor meerdere BMS kunnen aansluiten.

Verder moet er informatie over het energieverbruik worden bijgehouden. Ook
moet informatie worden weergegeven op een display. Ook moet de informatie
beschikbaar zijn voor exporteren door een USB-interface.

Het systeem is tijdgericht en moet elke 5 minuten een datalogging uitvoeren,
ook het tijdgebonden communiceren van besturingsignalen.

Het besturingsysteem moet gevoed worden door het energieopslag systeem. Er Wordt
gewerkt met 24 Volt. Dit is een hoog voltage dat mogelijk lager moet.


### 3.7.3 Onderzoeksvraag
De onderzoeksvraag wordt beantwoord door de deelvragen te beantwoorden binnen de
context van de hoofdvraag en de eisen uit het rapport Programma Van Eisen.

 - Hoe kan een besturingsysteem het BMS systeem aansturen en sensor data verzamelen voor weergave?
  - Hoe kan het BMS systeem bestuurd worden?
  - Hoe kan sensor data digitaal verzameld worden?
  - Hoe kan data weergegeven worden?

Het doel van het besturingssysteem is het aansturen van verschillende hardware en het bijhouden van informatie. Het besturingsysteem communiceert met de andere systemen door hardware communicatie protocolen (e.g. I2C, SPI, I/O). Voor het uitlezen van energieverbruik is er communicatie met het DC/DC converter systeem nodig en het regelen van de energieopslag wordt bereikt door communicatie met het BMS.


### 3.7.4. Bestaande oplossingen
Om te bepalen welke producten gebruikt kunnen worden om dit systeem te ontwerpen
wordt er naar bestaande oplossingen gekeken. Het besturingsysteem zal een
microcontroller worden met verschillende randapparatuur. Een paar bedrijven die deze ontwerpen zijn,
Adafruit, Arduino, Espressif Systems, ST Microelectornics en Texas Instruments.

ST Microelectronics heeft een zonnepaneel systeem ontworpen.
[Products and solutions for solar energy](https://www.st.com/content/ccc/resource/sales_and_marketing/promotional_material/brochure/67/f0/0f/23/95/f2/43/d4/BRSOLAR.pdf/files/BRSOLAR.pdf/jcr:content/translations/en.BRSOLAR.pdf) en gebruikt voor 'Control units' StM32F serie microcontrollers.
In het [Electricity Metering](https://www.st.com/en/applications/metering/electricity-metering.html) ontwerp van ST Microelectronics wordt er gebruik gemaakt van een STM32F voor het besturen
van een DC/DC inverter systeem, [Data Concentration Unit](https://www.st.com/en/applications/metering/data-concentrator-unit.html).

![STM product offering tabel](figuren/stm_micro_tabel.png){ width=450px }  
Afbeelding 1: ST Microelectronics Control Units

In Afbeelding 1 is een overzicht van ST Microelectronics microcomputers en waar deze voor worden ingezet.

### 3.7.5. Hoe kan het BMS systeem bestuurd worden?
De aansturing van het BMS zal werken middels digitale en analoge
signalen. Een ADC converter kan analoge lezen. Digitale signalen werken middels
5V GPIO. Voor de data export is nodig. De eisen voor de BMS besturing zijn nog
niet bekend.

De processor keuze heeft invloed op de software, iedere processor heeft
verschillende ontwikkelings software om de besturing software te maken.

In Tabel 1 is een overzicht gemaakt van veel gebruikte microcontrollers met
de relevante eigenschappen.

| product | Prijs (EUR) | Processor | ADC | GPIO | I2C | USB
|----------|---|---|--|--|--|--
| [STM32F407G-DISC1](https://nl.mouser.com/ProductDetail/STMicroelectronics/STM32F407G-DISC1?qs=sGAEpiMZZMvt1VFuCspEMrjE4TO0IyBBeNQ%252BgqeMpN8%3D) | 18,36 | Arm Cortex   | 3 | 138 | 3 | 2.0 Device
| Raspberry Pi 2 Model B  | 25,- | Arm Cortex-A7 | 0 |  40 | 1 | 2.0 Host
| Arduino Mega 2560 Rev3 | 35,- | ATmega2560 | 16  | 54 | 1  | 2.0 Device

Tabel 1: Microcontroller eigenschappen

### 3.7.6. Hoe kan sensor data digitaal verzameld worden?
De dataverameling wordt uitgevoerd door hardware communicatie. Het DC/DC link systeem zal met i2c of SPI uitgelezen worden. Het BMS systeem wordt uitgelezen zoals beschreven in het vorige hoofdstuk.

De data zal bestaan uit energieverbruik, batterij capaciteit en de status van de BMS onderdelen.

De datalogging bestaat uit verbuik en batterij capaciteit en de sample tijd is 5 minuten. Dit maakt de hoeveelheid data per dag: 8 Bytes * 720 momenten = 5760 Bytes. De data zal bestaan uit twee getallen. Het verbuik in vermogen en de lading van het BMS in percentage. Er wordt van 32 bits per getal uitgegaan.

USB 2.0 maakt het mogelijk om een microcontroller direct aan te sluiten aan een
computer. De data kan in ram worden bewaard, maar voor de mogelijkheid om meer data op te nemen kan een lange-termijn data opslag module worden gebruikt. Voor de data opslag kan een Micro SD-Card gebruikt worden.

in Tabel 2 is een overzicht van verschillende data opslag mogelijkheden.

| USB | Prijs (EUR) |
|-|-|
| [Micro SD TF Card Module](https://nl.aliexpress.com/item/33029367382.html) | 0,25
| [open-source data-logger](www.antratek.nl/openlog?gclid=EAIaIQobChMIiNOx6v3B5QIVBUkYCh0TgAtxEAQYASABEgJjkfD_BwE) | 18,09 |

Tabel 2: Data export mogelijkheden.


### 3.7.7. Hoe kan data weergegeven worden?
Voor het display wordt er verwacht dat informatie in de vorm van text en
getallen wordt weergegeven aan een gebruiker. Het Display moet groot genoeg
om het BMS (status, temperatuur en capaciteit) en het DC/DC converter systeem
(verbruik) informatie weer te geven.

Voor het aansturen van een display kan er een microcontroller gebruikt worden.
Een display heeft veel data overdracht en besturing nodig heeft, dit betekent
dat er veel datalijnen lopen. Een scherm gekozen
worden met een ingebouwde Driver. De driver wordt aangestuurd met I2C of SPI,
zodat de pixel buffer ingebouwd is in de Display.
De weergave data moet verversd worden voor gebruiker inzicht, de weergaven moet
dus niet vaak veranderen.

Voor een Display (zie Tabel 3) met driver heb je 2 soorten schermen: pixel schermen en symbool schermen.
Een pixel display heeft punten waar symbolen mee gevormd worden en kan
verschillende groottes gebruiken voor de symbolen. Terwijl een symbool display
bestaat uit een bepaalde hoeveelheid symbolen per regel, en een hoeveelheid regels.


| Display         | Prijs (EUR) | grootte | protocol
|-----------------|------|----------|-----
| [Diymore LCD2004](https://nl.aliexpress.com/item/32677443139.html) | 1,66 | 20x4 symbolen  | SPI
| [Diymore 2004 LCD](https://nl.aliexpress.com/item/32675169557.html)  | 2,38 | 20x4 symbolen  | I2C
| [Diymore SSD1309](https://nl.aliexpress.com/item/32871927644.html) | 12,67 | 128x64 pixels | I2C/SPI

Tabel 3: Display mogelijkheden.

### 3.7.8 Conclusies
Het besturingssysteem bestaat uit een microcontroller. Het besturingsysteem regelt de communicatie tussen de verschillende systemen, weergeeft de status informatie op een display en regelt de datalogging voor tijdgebonden data.

Het display staat aangesloten aan een microcontroller en geeft gegevens weer aan de gebruiker. Verder kan een gebruiker de data uitlezen door USB.


# 4. Project 2018
In de voorgaande hoofdstukken zijn de
Het PV-systeem dat wordt ontworpen voor de Hogeschool Rotterdam bestaat uit verschillende deelsystemen. In het voorgaande project is het proces uitgewerkt in een architectuurontwerp, zie figuur.

De deelsystemen zichtbaar in figuur ? worden beschreven in het project document. Het proces "Solar" meet de spanning en stroom van het zonnepaneel en stuurt de gemeten waardes door naar de DC-link, de opgewekte energie wordt getransporteerd naar het oplaadcircuit. Het proces "BMS" betreft het op- en ontladen van de accu's. Het oplaadcircuit maakt gebruik van verschillende methodieken om een langere levensduur van de accu's te garanderen, de methodieken zijn: Constant Current - Constant Voltage charging (CC-CV) en trickle charging. Het ontlaadcircuit verzorgt de gewenste spanning aan het grid en geeft de capaciteit van de accu's door aan de DC-link, om diep ontlading te voorkomen. Het proces "DC-link" wisselt tussen verschillende DC-bronnen, communiceert met verschillende onderdelen en bepaald welke bron het grid voedt. Verder is de DC-link verantwoordelijk voor het opladen en de beveiliging van de accu's. Het proces "Display" weergeeft data over het PV-systeem.
## 4.1. Reeds onderzocht
bespreek hier alle IC die in het vorig onderzoek besproken werden
## 4.2. Ontwerp keuzes
In de vorige periode van dit project zijn er een aantal ontwerp keuzes gemaakt aan de hand van het onderzoek. Sommige van deze keuzes blijven vast staan maar sommige kunnen of zullen juist veranderen.


## 5. project 2019
# 6. Conclusies
##6.1 project 2018 vs project 2019
Wat zijn de drastische onwerp veranderingen die we doorgaan voeren?

# Bronnen

[1] Jinju Zhou, Lina He, Canbing Li, Yijia Cao, Xubin Liu, and Yinghui Geng,   *What's the Difference between Traditional Power Grid and Smart Grid ? — from Dispatching Perspective*, College of Electrical and Information Engineering, HMunan University, Changsha, China, December 2013

[2] Nils van der Blij, Pavel Purgat, *DC Distribution Smart Grids*, https://www.tudelft.nl/ewi/over-de-faculteit/afdelingen/electrical-sustainable-energy/dc-systems-energy-conversion-storage/research/dc-distribution-smart-grids/, geraadpleegd op: 04-10-2019

[S1] Pollefliet - Elektronische vermogenscontrole

[S2] Rashid - Power Electronics Handbook

[S3] Power point presentaties VEE02 Fogarty

[S4] Datasheet LT3840

# Bijlagen
