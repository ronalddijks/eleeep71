## Informatie

|                    |                  |
| ------------------ | ---------------- |
| Projectleider      | Stefan Zuithoff  |
| Projectleden       | Nathan Godefroij |
|                    | Pablo Passchier  |
|                    | Randy Jacobs     |
| Begeleider         | J.G. Straver     |
| Datum van uitgifte | 17-10-2019       |

## Versiehistorie

| Versie | Datum      | Wijzigingen              | Auteur       |
| ------ | ---------- | ------------------------ | ------------ |
| 1.0    | 10-09-2019 | Opzet, invullen van hoofdstukken | Projectgroep |
| 1.1    | 17-10-2019 | Toevoeging van deelsystemen | Projectgroep |
| 1.2    | 17-10-2019 | besturingsysteem ingevuld | Pablo Passchier |
