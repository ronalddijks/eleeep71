Omdat er nog geen duidelijk rapport structuur aanwezig is, en ieder zijn eigen onderzoek doet, heb ik hier een document dat kan helpen. Het doel is om volgende week een duidelijk rapport structuur op te zetten zodat het werk wat overzichtelijker wordt. voel je vrij dit aan te passen, dit is enkel bedoelt als hulpmiddel.


Dit is de Inhoudsopgave van het onderzoek dat vorig jaar gedaan is. Zie dit als een lijddraad voor wat te onderzoeken

Inhoudsopgave
Versiebeheer  
1. Vooronderzoek energieopslag
2. Ingangsenergie  
  2.1 Zonnepaneel  
  2.2 230V-net  
3. Batterij  
  3.1 Type batterijen  
  3.2 Op-/ontladen  
  3.3 Batterij beveiliging  
  3.4 Single or Dual battery  
4. Uitgangsenergie  
  4.1. Terugleveren
  4.2 DC-link  
  4.3 Intern gebruik
5. Methode
  5.1 Situatie 1
  5.2 Situatie 2
  5.3 Situatie 3
6. Configuratie
7. Algoritme energieopslag
8. Voeding als zonnepaneel (Fotovoltaïsch simulator)
Verwijzingen

Wat kan er onderzocht worden?
  - smart grids
  - battery management
  - Microcontrollers
    - datalogging
    - display weergave
  - Weergave systemen
  - Converter DC DC
  - DC- grids
  - Lead-Acid vs Lithium-Ion
