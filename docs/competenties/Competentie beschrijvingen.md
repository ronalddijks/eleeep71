# 1.	Analyseren
Het	analyseren	van	een	engineeringvraagstuk	omvat	de	identificatie	van	het	probleem	of
klantbehoefte,	de	afweging	van	mogelijke	ontwerpstrategieën	/	oplossingsrichtingen	en	het
eenduidig	in	kaart	brengen	van	de	eisen	/	doelstellingen	/	randvoorwaarden.	Hierbij	wordt
een	scala	aan	methoden	gebruikt,	waaronder	wiskundige	analyses,	computermodellen,
simulaties	en	experimenten.	Randvoorwaarden	op	het	gebied	van	mens	&	maatschappij,
gezondheid,	veiligheid,	milieu	&	duurzaamheid	worden	hierbij	meegenomen.

# 2.	Ontwerpen
Het	realiseren	van	een	engineeringontwerp	en	hierbij	kunnen	samenwerken	met	ingenieurs
en	niet-ingenieurs.	Het	te	realiseren	ontwerp	kan	voor	een	apparaat,	een	proces	of	een
methode	zijn	en	kan	meer	omvatten	dan	alleen	het	technisch	ontwerp,	waarbij	de	engineer
een	gevoel	heeft	voor	de	impact	van	zijn	ontwerp	op	de	maatschappelijke	omgeving,
gezondheid,	veiligheid,	milieu,	duurzaamheid	(bijv.	cradle-to-cradle)	en	commerciële
afwegingen.	De	engineer	maakt	bij	het	opstellen	van	zijn	ontwerp	gebruik	van	zijn	kennis
van	ontwerpmethodieken en	weet	deze	toe	te	passen.	Het	te	realiseren	ontwerp	is
gebaseerd	op	het	programma	van	eisen	en	vormt	een	volledige	en	correcte	implementatie
van	alle	opgestelde	producteisen.

# 3.	Realiseren
Het	realiseren	en	opleveren	van	een	product	of	dienst	of	de	implementatie	van	een	proces
dat	aan	de	gestelde	eisen	voldoet.	De	Engineer	ontwikkelt	hiervoor	praktische	vaardigheden
om engineeringproblemen	op	te	lossen	en	voert	hiervoor	onderzoeken	en	testen	uit.	Deze
vaardigheden	omvatten	kennis	van	het	gebruik	en	de	beperkingen	van	materialen,
computer	simulatie	modellen,	engineeringprocessen,	apparatuur,	praktische	vaardigheden,
technische	literatuur	en	informatiebronnen.	De	bachelor	is	ook	in	staat	om	de	wijdere
(veelal	niet-technische)	gevolgen	te	overzien	van	zijn	werkzaamheden,	bijv.	op	het	gebied
van	ethiek,	maatschappelijke	omgeving,	duurzaamheid,	commercie	en	industrie.

# 4.	Beheren
Het	optimaal	laten	functioneren	van	een	product,	dienst	of	proces	in	zijn	toepassingscontext
of werkomgeving,	rekening	houdend	met	aspecten	op	het	gebied	van	veiligheid,	milieu,
technische	en	economische	levensduur.

# 5.	Managen
De	Engineer	geeft	richting	en	sturing	aan	organisatieprocessen	en	de	daarbij	betrokken
medewerkers	teneinde	de	doelen	te	realiseren	van	het	organisatieonderdeel	of	het	project
waar	hij	leiding	aan	geeft.

# 6.	Adviseren
De	Engineer	geeft	goed	onderbouwde	adviezen	over	het	ontwerpen,	verbeteren	of
toepassen	van	producten,	processen	en	methoden	en	brengt	renderende	transacties	tot
stand	met	goederen	of	diensten	binnen	het	Domein	Engineering.

# 7.	Onderzoeken
Het	gebruik	van	geschikte	methoden	en	technieken	m.b.t.	het	vergaren	van	informatie,	om
toegepast	onderzoek	uit	te	kunnen	voeren.	Deze	methoden	kunnen	zijn:
literatuuronderzoek,	het	ontwerp	en	de	uitvoering	van	experimenten,	de	interpretatie	van
data	en	computer	simulaties.	Hiervoor	kunnen	databases,	normen,	standaarden	en
veiligheidsnormen	geraadpleegd	worden.

# 8.	Professionaliseren
Het	zich	eigen	maken	en	bijhouden	van	vaardigheden	die	benodigd	zijn	om	de	overige
engineeringcompetenties	effectief	uit	te	kunnen	voeren.	Deze	vaardigheden	kunnen	ook	in
breder	verband	van	toepassing	zijn	en	omvatten	ook	het	op	de	hoogte	zijn	van	de	nieuwste
ontwikkelingen,	ook	in	relatie	tot	ethische	dilemma’s	en	maatschappelijk	geaccepteerde
normen	en	waarden.
