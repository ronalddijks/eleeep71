Hier wordt de begroting van het project bijgehouden inclusief de aankopen en het restrerende budget. Het budget voor dit project is 250 euro en zaak is om binnen dit budget te blijven.

Hieronder volgt een tabel waar de verandering van budget weergegeven wordt. De datum, restrerende budget en de aankoop die de verandering in budget veroorzaakt heeft worden hier onder opgenomen.

Datum      | Budget  | Aankoop
-----------|---------|---------
18-10-2019 | €250,-  | geen



Hieronder volgt een tabel waar de aankopen zijn opgegeven. De datum van aankoop, korte bescrijving, bedrag en systeemcomponent

Datum      | Bedrag  | Beschrijving   | Component  
-----------|---------|----------------|----------
           |         |                |


Hieronder volgt de begroting per functionele component

Conponent  | Budget           |
-----------|------------------|
BMS        | €30  - €50       |
Besturing  | €30  - €50       |
Buck 5V    | €5   - €10       |
DC-DC link | €30  - €50       |
LCD        | €10  - €15       |
Andere     | €30  - €40       |
**Totaal** |**€135 - €215**   |
