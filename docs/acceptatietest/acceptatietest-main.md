---
title: 'Minor Project: Energieopslag - Acceptatiest'
---


# 1. Inleiding
In dit document worden testen beschreven welke aantonen dat een product voldoet aan het Programma Van Eisen. Er zijn per deelsysteem van het eindproduct requirements opgesteld.

Elke test wordt in een voorbereide omgeving uitgevoerd.

## 1.2. Doel
Het opslaan en converteren van elektrische energie. De elektriciteit is afkomstig van zonnepannelen en kan in verschillende maten ontvangen worden door een DC grid.


# 2. Voorbereiding acceptatietest
Er wordt een testomgeving gebruikt welke specifiek voorbereid wordt om alle
acceptatietests in uit te voeren.

## 2.1. Technopark
Het Technopark is de omgeving waar het systeem voor ontwikkeld is.

### 2.1.1. Hardware
Het energieopslag systeem wordt aangesloten aan de voeding en het technolab.
De voeding wordt aangesloten aan de DC/DC converter. het technopark wordt ook op
de DC/DC converter aangesloten.

### 2.1.2. Software
De software voor het uitlezen van de ge-exporteerde data moet op een computer
staan. Deze software is beschikbaar in de het bitbucket source control systeem.
De software heeft een installatie script welke uitgevoerd kan worden.

### 2.1.3 Overige voorbereiding
Er moet een leraar aanwezig zijn wanneer de accu's aangesloten worden.

# 3. Acceptatietestbeschrijving
Klanteisen worden in verschillende tests gecontroleerd. een test kan meerdere
klanteisen behandelen en een klanteis kan in meerdere tests voorkomen. De test
slaagd wanneer de criteria voltooid wordt. Voor elke test is er een procedure
Welke uitgevoerd moet worden om de criteria te bereiken.

## 3.1 Test 1

## 3.1.1 Under Charge protection

Tabel 1: voorbeeld

klanteis(en)  | BMS001
---|-----
Voorwaarden   | Testomgeving: Technopark.
    | 1 Accu geeft meer dan 9V aan.
    | voeding staat uit
Testinvoer    | DC grid wordt aangesloten
Testuitvoer   |
Criteria      | het BMS systeem gaat uit voordat Accu onder 9V komt.
Testprocedure | Sluit het Grid aan. Wacht totdat de Accu naar 9V gaat.

## 3.1.2 Over Charge Protection

Tabel 1: voorbeeld

klanteis(en)  | BMS002
---|-----
Voorwaarden   | Testomgeving: Technopark. 1 Accu geeft minder dan 14V aan.\nstaat\nDC grid staat uit
Testinvoer    | DC grid wordt aangesloten
Testuitvoer   |
Criteria      | het BMS systeem gaat uit voordat de Accu boven 14V gaat.
Testprocedure | Sluit het Grid aan. Wacht totdat de Accu naar 14V gaat.

# 4. Traceerbaarheid klanteiesen

                        | BMS001 | BMS002 | BMS003 | BMS004 | BMS005 | BES001
-|-|-|-|-|-|-
Over Charge Protection  |   x    |        |        |        |        |        
Under Charge protection |        |    x   |        |        |        |        
                        |        |        |        |        |        |        
                        |        |        |        |        |        |        
                        |        |        |        |        |        |        
