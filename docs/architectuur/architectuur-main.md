# 1. Architectuur
In dit hoofdstuk wordt het architectuurontwerp voor dit project toegelicht. Het architectuurontwerp bestaat uit een context diagram en een interconnect diagram. Het context diagram weergeeft het globale systeem en de flows naar externe partijen. Het interconnect diagram weergeeft de deelsystemen en de flows tussen de deelsystemen. Beide diagrammen worden beschreven, dit geeft meer duidelijkheid over de integratie van verschillende deelsystemen.

## 1.1 Context diagram
In figuur 1 wordt het context diagram weergegeven. Het context diagram is het hoogste level van het systeem en beschrijft de interactie tussen het systeem en zijn omgeving.

![Context diagram](contextdiagram.png)
**Figuur 1** - Context diagram van project energieopslag

De interacties tussen het systeem en omliggende partijen kan als volgt beschreven worden:
- De systeemstatus kan door de gebruiker afgelezen worden. De gebruiker kan ook de geschiedenis van het systeem inzien.
- De gebruiker kan het systeem aan- of uitzetten.
- De DC-grid van het technolab functioneert op 48V.
- Als de accu's in diepontlading dreigen te komen worden deze opgeladen via het net, de omvormer levert 48V
- De accu's leveren een spanning van 24V, voor het opladen van de accu's moet de spanning omgevormt worden naar 24V.
- De zonnepanelen worden gesimuleerd met een steady DC voltage.

## 1.2 Interconnect diagram
In figuur 2 wordt het interconnect diagram weergegeven. Het interconnect diagram gaat een level dieper dan het context diagram, het weergeeft de deelsystemen en de interactie tussen deelsystemen.

![Context diagram](systeemdiagram.png)  
**Figuur 2** - Interconnect diagram van het energieopslag systeem

De processen van deelsystemen worden beheerd met behulp van software in het besturingsysteem.
In Figuur 3 is een

## 1.3 Process Beschrijving
Voor de processen welke worden ontwikkeld als software is er in Figuur 3 een functionele overzicht van de processen. Het programma zal communicatie moeten voorbereiden, waarna het constant communiceert met andere systemen en beslissingen maakt per deelsysteem.

![software process beschrijving](software_architectuur.png)  
**Figuur 3** - software process beschrijving
<!-- bron: https://drive.google.com/file/d/1lXevmweRDkMQGKEasYK3hAF9oY2sO2eE/view?usp=sharing -->

## 1.4 Beschrijving deelsystemen
Hieronder volgt een beschrijving van de deelsystemen die weergegeven worden in het interconnect diagram. Het gaat om de functionaliteit van het systeem, de ontwerpmethode en het realisatie proces worden toegelicht in het onderzoek en uitgewerkt in het detailontwerp.

### 1.4.1 BMS
Verantwoordelijk voor het controleren van de status van de accu's. Hierbij behoort het beschremen tijdens het op- en ontladen, monitoren van externe factoren (temperatuur) en het leveren van data aan het besturing systeem. Met behulp van de BMS kunnen de accu's in een veilige omgeving en toestand functioneren.

### 1.4.2 DC/DC Converter
Verantwoordelijk voor het schakelen tussen verschillende spanningsniveau's in het netwerk. Er wordt zover bekend gebruik gemaakt van 4 24Ah 12V AGM solar accu's. De aanbevolen configuratie van de accu's is 2s2p, dit betekent 2 keer de capaciteit en 2 keer de spanning (24V). Het technolab werkt met 48V, de DC/DC converter is het schakelpunt tussen de verschillende systemen.

### 1.4.3 Besturingsysteem
Verantwoordelijk voor het monitoren van het netwerk. Het is van belang dat de gebruiker de huidige status van het systeem kan observeren. Het ontvangen, verwerken, uitsturen en loggen van data zijn allemaal taken van het besturing systeem.

### 1.4.4 Display
Verantwoordelijk voor het weergeven van de huidige systeem status. De weergegeven informatie bestaat o.a. uit de state of charge, de accu status en de accu temperatuur

## 1.5 Beschrijving van de interconnecties
Hieronder volgt een beschrijving van de interconnecties tussen de deelsystemen. De beschrijving betreft de functionaliteit en niet de exacte methode, deze wordt later toegelicht.

### 1.5.1 Interconnectie tussen BMS en DC/DC converter
De interconnectie tussen de BMS en DC/DC converter bestaat uit een stabiele 24V DC.

### 1.5.2 Interconnectie tussen BMS en Besturingsysteem
De interconnectie tussen de BMS en besturing systeem bestaat uit het overdragen van gegevens over de huidige accu status.

### 1.5.3 Interconnectie tussen DC/DC converter en Besturingsysteem
De interconnectie tussen de DC/DC converter en het besturing systeem bestaat uit een data verbinding. Het besturing systeem monitoort de verbinding tussen de DC/DC converter en het technolab.

Ook wordt het besturingsysteem gevoed uit de DC/DC converter.

### 1.5.4 Interconnectie tussen Besturingsysteem en Display
De interconnectie tussen de besturing systeem en display bestaat uit het overdragen van gegevens die de huidige status van het systeem weergeven.

### 1.5.5 Interconnectie tussen Besturingsysteem en Gebruiker
De gebruiker kan de display aan/uit zetten met een knop.

### 1.5.6 Interconnectie tussen gebruiker en Display
De gebruiker kan het display uitlezen.

### 1.5.7 Interconnectie tussen DC/Dc converter en Technolab
De interconnectie tussen de DC/DC converter en het technolab bestaat uit een voedingskabel.

### 1.5.8 Interconnectie tussen voeding en DC/DC converter
De interconnectie tussen de voeding en de DC/DC converter bestaat uit een voedingskabel.

### 1.5.9 Interconnectie tussen BMS en Accu
De interconnectie tussen de BMS en Accu zal bestaan uit een op maat gekocht aansluiting.
