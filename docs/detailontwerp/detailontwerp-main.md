
# inleiding  
In dit hoofdstuk wordt het gedetailleerd ontwerp van de in paragraaf 3.1.2 gedefinieerde deelsystemen beschreven.

# 1. Detailontwerp  
Nadat de eisen die aan het systeem worden gesteld zijn vastgelegd in het programma van eisen, en de specificaties van de units (deelsystemen die niet verder opgesplitst zijn) zijn vastgelegd in het architectuurontwerp (paragraaf 3.1), kan begonnen worden met het ontwerpen van de units. De specificaties definiëren wat elke unit moet doen, bij het ontwerpen wordt bepaald hoe deze unit dit gaat doen. Het is de bedoeling dat het ontwerp van elke unit zover gedetailleerd wordt, dat de bouwer de unit ook daadwerkelijk kan bouwen (de schakeling realiseren en/of de software schrijven).

## 1.1 Hardware ontwerp
### 1.1.1 BMS  
De uitwerking dient zodanig te zijn dat op basis van deze uitwerking de unit geïmplementeerd kan worden.

Voor een hardware unit kun je in deze paragraaf bijvoorbeeld het volgende plaatsen:  
•Een beschrijving van de implementatie van de unit. Vergeet niet om te verwijzen naar eventuele bronnen en om de gemaakte keuzes te onderbouwen. De implementatie van een unit wordt vaak gebaseerd op een referentieontwerp uit een datasheet of op een voorbeeld uit een boek of van het internet.  
•Een schema van de unit en uitleg over de werking ervan.  
•Berekeningen aan componenten en dergelijke.  
Voor een unit die software bevat of alleen uit software bestaat kun je in deze paragraaf bijvoorbeeld het volgende plaatsen:  
•Een beschrijving en uitleg van de gebruikte algoritmen. Vergeet niet om te verwijzen naar eventuele bronnen.  
•Een toestandsdiagram die het gedrag van de unit beschrijft.  
•Een structure chart met de aanroepvolgorde van de diverse functies waaruit de unit bestaat.  
•Een flowchart die de implementatie van een functie of unit beschrijft.  
•Een beschrijving van de belangrijkste functies waaruit de unit bestaat met hun invoer en uitvoer.  

### 1.1.2 DC/DC Converter


### 1.1.3 Besturingsysteem en display
Ontwerpkeuzes
Stroom toevoer
Vermogen lezing
BMS aansturing
BMS uitlezen


### 1.1.4 Display  

## 1.2 Software Ontwerp
