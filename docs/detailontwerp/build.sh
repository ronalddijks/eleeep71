pandoc \
    -V linkcolor:blue \
    -V geometry:a4paper \
    -V geometry:margin=2cm \
    -V documentclass=report \
    -M lang=nl \
    --toc \
    -o detail_ontwerp.pdf \
    detail_ontwerp-front.md \
    detail_ontwerp-main.md
