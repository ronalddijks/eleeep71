pandoc \
    -V linkcolor:blue \
    -V geometry:a4paper \
    -V geometry:margin=2cm \
    -V documentclass=report \
    -M lang=nl \
    --toc \
    -o pve.pdf \
    pve-front.md \
    pve-main.md
