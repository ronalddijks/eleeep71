
## Informatie

|                    |                  |
| ------------------ | ---------------- |
| Projectleider      | Stefan Zuithoff  |
| Projectleden       | Nathan Godefroij |
|                    | Pablo Passchier  |
|                    | Randy Jacobs     |
| Begeleider         | J.G. Straver     |
| Datum van uitgifte | 15-10-2019       |

## Versiehistorie

| Versie | Datum      | Wijzigingen              | Auteur       |
| ------ | ---------- | ------------------------ | ------------ |
| 1.0    | 12-09-2019 | Eerste versie. | Pablo Passchier, Randy Jacobs|
| 1.1    | 26-09-2019 | Aanpassing na Feedback | Nathan Godefroij|
| 1.2    | 03-10-2019 | Verbetering aan 'Systeemcontext' en structuur | Pablo Passchier |
| 1.3    | 03-10-2019 | Aanpassingen systeemcontext en requirements | Stefan Zuithoff |
| 1.4    | 09-10-2019 | Systeemcontext en requirements (feedback) | Pablo Passchier |
| 1.5    | 15-10-2019 | Requirements besuringsystemen en DC/DC Converter (klant feedback) | Pablo Passchier, Stefan Zuithoff |
