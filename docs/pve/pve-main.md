---
title: 'Minor Project: Energieopslag - Programma van Eisen'
---


# 1.  Algemene beschrijving
 De Hogeschool Rotterdam heeft opdracht gegeven om een energieopslag systeem te realiseren.vanuit de Hogeschool zijn er bepaalde eisen gesteld waar de energieopslag systeem aan moet voldoen. Deze eisen worden geformaliseerd in dit document. Circa de helft van de eisen zijn direct afkomstig van de klant. Deze eisen zijn vooral opgesteld om de scope van het project te bepalen, binnen deze eisen blijft nog veel vrijheid over. Echter zijn er additionele eisen die voortkomen uit de vorige fase van dit project. Deze eisen zijn overgenomen vanwege bepaalde ontwerp keuzes die al vaststaan.

Er wordt verwacht dat er een nieuw ontwerp komt welke voldoet aan de eisen van functionaliteit en veiligheid. Verder moet er een display komen welke informatie weergeeft over het huidige verbruik en de huidige lading. De zonnepannelen vallen buiten het ontwerp, maar worden wel meegenomen als een deelsysteem.

Dit document beschrijft de context van het systeem en de eisen voor het systeem.

# 2.  Systeemcontext
Het systeem dat geproduceerd moet worden is een energieopslag systeem bestaande uit accu's, het wordt aangesloten aan zonnepanelen en een DC grid. De  De functionaliteit van de verschillende systemen worden hieronder beschreven.

De ingang van het systeem (zie: Figuur 1) zijn de zonnepanelen en de uitgang van het systeem is het technopark. Vanwege de sterk variërende beschikbaarheid van energieproductie uit zonnenpanelen en het wisselende verbruik van energie in het technopark, zal het opslag systeem nodig zijn om de vraag aan energie te kunnen leveren. Daarom wordt de elektrische energie tussen de ingang en uitgang van het systeem, beheerd en gemeten.

![Context diagram](contextdiagram.png)  
**Figuur 1** - Context diagram van project energieopslag

Hieronder worden deelsystemen beschreven welke buiten de scope van het project vallen. Deze deelsystemen zijn extern gespecificeerd en worden hier voor context uiteengezet. Deze deelsystemen hebben geen requirements binnen dit project, maar bepalen deels de requirements voor de componenten die wel in de scope vallen.

### 2.1. zonnepanelen
De stroom toevoer voor het DC-grid wordt geproduceerd door zonnepanelen. Zonnepanelen maken gebruik van een serie van zonnecellen die zonlicht omzetten in elektriciteit. De energie productie van een zonnecel is voornamelijk afhankelijk van de lichtintensiteit, richting van inval en temperatuur.

### 2.2. Accu
De Accu is een passief systeem welk wordt aangesloten op een Battery Management System. De Acu wordt gevoed vanuit het BMS en kan ontladen worden naar het grid. De eisen en specificaties van de Accu zijn voorafgaand aan dit project bepaald.

### 2.3. Solar Charge Controller
Een Solar Charge Controller (SCC) is een circuit dat de stroom toevoer vanaf de zonnepanelen regelt. De energie productie in een zonnepaneel is sterk variabel door verschillende condities, waaronder temperatuur, hoeveelheid en richting van licht inval. De SSC kan deze stroom toevoer regelen zodat de zonnepaneel meest efficiënt belast wordt. Twee verschillende toepassingen kunnen hiervoor gebruikt worden, dat zijn Pulse Width Modulation (PWM) en Maxiumun Power Point Tracking (MPPT). In meeste gevallen behaald MPPT de hoogste efficiëntie in vergelijking met PWM.

# 3. Systeemfuncties
De systeemfuncties binnen de scope van dit project worden beschreven met de bijbehorende requirements. Dit zijn: Battery Management System (BMS), Besturingssysteem en de DC/DC converter.

### 3.1. Battery Management System
Aangesloten aan het SCC is het Battery Management System (BMS). De BMS gebruikt de power van de SCC om de Accu's te kunnen opladen. De BMS regelt de afzonderlijke Accu's op verschillende condities waaronder temperatuur, state of charge, state of discharge. De status informatie van de Accu's worden gecommuniceerd aan het besturingsysteem.

In het BMS systeem wordt er geschakeld tussen de ingangen om te voorkomen dat de accu's in diepontlanding komen. Het systeem moet gelijktijdig energie kunnen leveren als ontnemen van de accu's.


Eisnummer|Eis | Type | Beschrijving
-|---|---|-----|
BMS001 | Under charge protection | Functioneel | Tijdens het ontladen van de accu's mogen individuele accu's niet onder de 9V komen. |
BMS002 | Over charge protection | Functioneel | Tijdens het opladen van de accu's mogen individuele accu's niet over 14V. |
BMS003 | Temperature control| Functioneel |Tijdens het functioneren van de accu's mogen alle accu's niet onder de 10 °C en niet boven de 30 °C komen |
BMS004 | Efficiënt opladen | Functioneel | Opladen moet van zonnepanneel naar accu moet een rendement halen van 90%
BMS005 | Diepontlading | Functioneel |  De capaciteit van de accu mag niet onder de 40% komen.


### 3.2. Besturingssysteem
Het besturingsysteem beheert de verschillende deelsystemen. Het besturingssysteem bestaat uit een microcontroller die geprogrammeerd wordt met de besturingsprocessen, ook zal er hardware bij komen voor het communiceren met andere systemen. Het energieverbuik wordt bijgehouden
door  het besturingsysteem. Het energieverbruik kan ge-exporteerd worden en geeft gegevens weer aan gebruikers door een display.


Eisnummer | Eis | Type | Beschrijving
-|---|---|-----|
BES001 | beschikbaarheid | Functioneel | Het besturingsysteem trekt stroom vanuit het BMS systeem, zodat het eindproduct autonoom is.
BES002 | Monitoren grid verbruik | Functioneel | Het vermogen naar het grid wordt elke 5 minuten bijgehouden.
BES003 | BMS beheren | Niet Functioneel | Het besturingsysteem stuurt het op- en ontlaad proces (BMS) aan, aan de hand van gegevens uit alle deelsystemen.
BES004 | weergave van grid verbruik | Functioneel | Wanneer het display aanstaat moet het display de (gemiddelde) accu capaciteit weergeven. De weergave moet in stappen van minimaal 5% en een accuratie van maximaal ± 5%.
BES004 | weergave van accu capaciteit | Functioneel | Wanneer het display aanstaat moet het display de (gemiddelde) accu capaciteit weergeven. De weergave moet in stappen van minimaal 5% en een accuratie van maximaal ± 5%.
BES005 | Weergave BMS data | Functioneel | Data over het BMS wordt weergegeven: De lading in Wh/s en de temperatuur in graden Celsius.
BES006 | Data export | Functioneel | Data van de afgelopen maand is beschikbaar door export via USB.

### 3.3. DC/DC Converter
Een schakelschema tussen de DC netwerken en een DC spanning converter. De DC stroom komt binnen uit de zonnepannelen en het accu systeem; de uitvoeren gaan naar het accu systeem of het DC grid.

De DC/DC converter omvormt een lagere spanning naar een hogere spanning en vice versa. Dit gebeurd in twee richtingen omdat er gelijktijdig energie geleverd als ontnomen wordt.

Eisnummer|Eis | Type | Beschrijving
-|---|---|-----
DCC001 | kortsluitingbeveiliging | Functioneel | De uitgansstroom bedraagt niet meer dan 12A. Bij een overschrijding hier van dient het systeem tijdig uit te schakelen.
DCC002 | Omvormen spanning | Functioneel | De DC/DC converter moet in staat zijn om tijdens het functioneren van het systeem de spanning over te kunnen zetten van 48V DC naar 24V.
DCC003 | Rendement | Functioneel | Het rendement van de DC/DC converter is minimaal 90%.
DCC004 | Uitgangsspanning rimpel | Niet Functioneel | De rimpel van de uitgangsspanning is gelijk of minder dan 1%.
DCC005 | Stroom rimpel | Niet Functioneel | De rimpel van de stroom door de spoel aan de uitgang bedraagt minder dan 20%.
